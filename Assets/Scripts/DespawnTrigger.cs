﻿using UnityEngine;
using System.Collections;

public class DespawnTrigger : MonoBehaviour 
{
	private Spawner _spawner;

	void Start()
	{
		_spawner = GameObject.FindObjectOfType<Spawner> ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Block") {
			if (other.gameObject.transform.GetComponent<Block>().isDraggable()) {
				_spawner.despawn(other.gameObject);
			} else {
				_spawner.spawnStop();
				GameManager.instance.endGame();
			}
		}
	}
}
