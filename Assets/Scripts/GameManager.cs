﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour 
{
	public static GameManager instance;

	public Spawner spawner;
    public float randomSeed = 0;

	public Gradient blockGradient;
	public Gradient backgroundGradient;

	//Private
	[SerializeField]
	private SortedDictionary<string, int> _highScores = new SortedDictionary<string, int>();
	[SerializeField]
	private int _score;
	private int _newHighscoreValue = -1;

	void Start()
	{
        Camera.main.backgroundColor = backgroundGradient.Evaluate(Random.value);
		loadScores ();
    }

	void Awake ()
	{
		if (!instance) 
		{
			instance = this;
		} 
		else if (instance != this)
		{
			Destroy(gameObject);
		}

        randomSeed = Random.seed ;
	}

	//----------------------
	//		GAME STATE
	//----------------------
	public void startGame()
	{
		spawner.spawnInit();

	}

	public void endGame()
	{
		_newHighscoreValue = -1;
		//Check current score.
		if (_score > 0) 
		{
			int i = 0;
			foreach (KeyValuePair<string, int> valuePair in _highScores.OrderByDescending(key => key.Value)) 
			{
				if (_score > valuePair.Value)
				{
					//New highscore.... at pos i bring up the gui...
					_newHighscoreValue = _score;
					break;
				}
				i++;
			}
		}

		PlayerPrefs.SetInt ("lastScore", _score);
		GUIController.instance.setLastScoreText (_score);

		//Now that we have saved the score, reset it..
		setScore(0);
		
		if (_newHighscoreValue > 0) 
			GUIController.instance.setGUIState (guiState.NEW_HIGHSCORE);
		else
			GUIController.instance.setGUIState (guiState.PAUSED);

		restartGame();
	}

	void restartGame()
	{
		GameObject.FindObjectOfType<PlayerController> ().lastPlacedBlock =
			GameObject.FindGameObjectWithTag ("Ground");
		float gradEva = Random.Range (1, 10);
		Camera.main.backgroundColor = backgroundGradient.Evaluate (gradEva / 10);
		spawner.despawnAll();
	}

	//----------------------
	//		 SCORING 
	//----------------------
	public void loadScores()
	{
		GUIController.instance.setLastScoreText (PlayerPrefs.GetInt ("lastScore", 0));
		
		for (int i = 0; i < GUIController.instance.highscoreTexts.Length; i++)
		{
			_highScores.Add(PlayerPrefs.GetString ("highScoreName"+i, i+""), PlayerPrefs.GetInt ("highScore"+i, (1+i*2)));
		}

		GUIController.instance.setHighscoresText (_highScores);
	}

	public void saveHighscores()
	{
		int i = 0;
        int maxScores = GUIController.instance.highscoreTexts.Length;
		foreach (KeyValuePair<string, int> valuePair in _highScores.OrderByDescending(key => key.Value)) 
		{
            if (i >= maxScores)
            {
                _highScores.Remove(valuePair.Key);
            }
            else
            {
                PlayerPrefs.SetInt("highScore" + i, valuePair.Value);
                PlayerPrefs.SetString("highScoreName" + i, valuePair.Key);
            }
			i++;
		}
	}

	public void setScore(int value)
	{
		if (_score == value)
			return;

		_score = value;

		//Update the GUI.
		GUIController.instance.setScoreText (value);
	}

	public void saveNewHighscore(string name)
	{
		string endName = name;
		if (_highScores.ContainsKey (endName)) 
		{
			int index = 1;
			while (_highScores.ContainsKey (endName))
			{
				endName = name + " (" + index+")";
				index ++;
			}
		}

		_highScores.Add (endName, _newHighscoreValue);
        
		GUIController.instance.setHighscoresText (_highScores);
		saveHighscores ();
	}

	public int getScore()
	{
		return _score;
	}
}
