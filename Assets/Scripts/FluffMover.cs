﻿using UnityEngine;
using System.Collections;

public class FluffMover : MonoBehaviour 
{
	public float distMin, distMax;
	public float speedMin, speedMax;

    bool lerptomax;
    float speed;

    void Start()
    {
        lerptomax = Random.value > 0.5;
        speed = Random.Range(speedMin, speedMax);
    }

	void Update ()
	{
		if (gameObject.transform.position.x > (distMax - 1f)) {
			lerptomax = false;
		}
		if (gameObject.transform.position.x < (distMin + 1f)) {
			lerptomax = true;
		}

		if (lerptomax) {
			gameObject.transform.position = Vector3.Lerp (gameObject.transform.position,
			                                              new Vector3(distMax, gameObject.transform.position.y, gameObject.transform.position.z),
			                                              Time.deltaTime * speed);
		} else {
			gameObject.transform.position = Vector3.Lerp (gameObject.transform.position,
			                                              new Vector3(distMin, gameObject.transform.position.y, gameObject.transform.position.z),
			                                              Time.deltaTime * speed);
		}

	}
}
