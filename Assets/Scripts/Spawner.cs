﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Spawner : MonoBehaviour 
{
	public Stack <GameObject> pool = new Stack<GameObject>();

	public Vector3 poolPos;

	public GameObject blockPrefab;

	public float spawnBaseRate;
	public float spawnMin;
	public float spawnIncreaseSpeed;
	private float _spawntime;
	private float _timer;

	public float minForce, maxForce;

    private bool _spawn;

    public Vector3 forceVector1;
    public Vector3 forceVector2;
    public Transform spawnPoint1;
    public Transform spawnPoint2;

    public void spawnInit()
	{
		_spawntime = spawnBaseRate;
		_spawn = true;
	}

	public void spawnStop()
	{
		_spawn = false;
	}

	public void despawn (GameObject block)
	{
		block.SetActive (false);
		block.transform.position = poolPos;
		pool.Push (block);
	}
	
	public void despawnAll()
	{
		GameObject [] Blocks = GameObject.FindGameObjectsWithTag ("Block");
		foreach (GameObject block in Blocks) {
			if (block.activeSelf) {
				despawn(block);
			}
		}
	}

	void blockInit(GameObject block)
	{
		float force = Random.Range (minForce, maxForce);
		Rigidbody rb = block.GetComponent <Rigidbody> ();

        Transform spawnPoint = Random.value > 0.5 ? spawnPoint1 : spawnPoint2;
        Vector3 forceVector = spawnPoint == spawnPoint1 ? forceVector1 : forceVector2;

        block.transform.position = spawnPoint.position;
		block.transform.rotation = Quaternion.Euler (Vector3.zero);
		block.SetActive (true);

        if (rb)
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(forceVector * force);
        }
	}

	void spawner ()
	{
		if (pool.Count >= 1) {
			GameObject poolBlock = pool.Pop ();
			blockInit (poolBlock);
		} else {
			GameObject newBlock = (GameObject)Instantiate (blockPrefab,
			                                               gameObject.transform.position,
			                                               Quaternion.identity);
			blockInit(newBlock);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (_spawn) {
			_timer += Time.deltaTime;
			if (_timer >= _spawntime) {
				if (_spawntime > spawnMin) {
					_spawntime -= spawnIncreaseSpeed;
				}
				spawner ();
				_timer = 0;
			}
		}
	}
}
