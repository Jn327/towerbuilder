﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
    public List<BlockDrag> blockDrags = new List<BlockDrag>();
    public GameObject lastPlacedBlock;

	void Start ()
	{
		lastPlacedBlock = GameObject.FindGameObjectWithTag ("Ground");
	}

	void Update()
	{
#if UNITY_EDITOR
		mouseInput ();
#else
		getTouchInput ();
#endif
		updateCameraPos ();
        if (Time.timeScale != 0 && blockDrags.Count > 0)
        {
            updateBlockDragsPos();
        }
	}

	void mouseInput()
	{
		if (Input.GetMouseButtonDown (0)) {
			Block hitBlock = null;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit rayHit;
		
			if (Physics.Raycast (ray, out rayHit, Mathf.Infinity)) {	
				if (rayHit.collider.gameObject.tag == "Selector") {
					hitBlock = rayHit.collider.gameObject.GetComponentInParent<Block> ();
					//Debug.Log (hitBlock.isDraggable ());
				}
				if (hitBlock != null /*&& hitBlock.isDraggable ()*/) {
                    blockDrags.Add(new BlockDrag(hitBlock));
				}
			}
		}

		if (Input.GetMouseButtonUp(0))
        {
            blockDrags.Clear();
        }
	}
	
	private void getTouchInput()
	{
		foreach (Touch touch in Input.touches) 
		{
			if (touch.phase == TouchPhase.Began)
			{
				Block hitBlock = null;
				Ray ray = Camera.main.ScreenPointToRay (touch.position);
				RaycastHit rayHit;
				
				if (Physics.Raycast (ray, out rayHit, Mathf.Infinity)) {	
					if (rayHit.collider.gameObject.tag == "Selector") {
						hitBlock = rayHit.collider.gameObject.GetComponentInParent<Block> ();
						//Debug.Log (hitBlock.isDraggable ());
					}
					if (hitBlock != null /*&& hitBlock.isDraggable ()*/) {
                        blockDrags.Add(new BlockDrag(hitBlock, touch));
					}
				}
			}
			else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
			{
                for (int i = 0; i < blockDrags.Count; i++)
                {
                    if (blockDrags[i].touch.fingerId == touch.fingerId)
                    {
                        blockDrags.RemoveAt(i);
                        break;
                    }
                }
            }
		}
	}

	private void updateCameraPos()
	{
		if (!lastPlacedBlock)
			return;

        if (lastPlacedBlock.tag == "Ground")
        {
            transform.position = Vector3.Lerp(transform.position,
                                                new Vector3(0.0f,
                                                             lastPlacedBlock.transform.position.y + 5.0f,
                                                             -12),
                                               Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position,
                                                new Vector3(0.0f,
                                                             lastPlacedBlock.transform.position.y + 1,
                                                             -12),
                                               Time.deltaTime);

        }
	}

	private void updateBlockDragsPos()
	{
#if UNITY_EDITOR 
		Vector3 screenPos = new Vector3 (Screen.width - Input.mousePosition.x,
		                                 Screen.height - Input.mousePosition.y,
		                                 Camera.main.transform.position.z);
		Vector3 currentPos = Camera.main.ScreenToWorldPoint (screenPos);
		currentPos.z = 0;
        for (int i = 0; i < blockDrags.Count; i++)
        {
            Vector3 currentPosDir = currentPos - blockDrags[i].block.gameObject.transform.position;
            blockDrags[i].block._rigidbody.velocity = (currentPosDir * 8);

            blockDrags[i].block._rigidbody.rotation = Quaternion.RotateTowards(blockDrags[i].block._rigidbody.rotation, Quaternion.identity, 100 * Time.deltaTime);
            blockDrags[i].block._rigidbody.angularVelocity = Vector3.zero;
        }
#else
		foreach (Touch touch in Input.touches) {
            for (int i = 0; i < blockDrags.Count; i++) 
            {
                if (touch.fingerId == blockDrags[i].touch.fingerId)
                {
				    Vector3 screenPos = new Vector3 (Screen.width - touch.position.x,
					                                 Screen.height - touch.position.y,
				                                     Camera.main.transform.position.z);
				    Vector3 currentPos = Camera.main.ScreenToWorldPoint (screenPos);
				    currentPos.z = 0;
                    Vector3 currentPosDir = currentPos - blockDrags[i].block.gameObject.transform.position;
                    blockDrags[i].block._rigidbody.velocity = (currentPosDir * 8);

                    blockDrags[i].block._rigidbody.rotation = Quaternion.RotateTowards(blockDrags[i].block._rigidbody.rotation, Quaternion.identity, 100 * Time.deltaTime);
                    blockDrags[i].block._rigidbody.angularVelocity = Vector3.zero;
                }
            }  
		}
#endif
    }
}
