﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public enum guiState
{
	NEW_HIGHSCORE,
	PAUSED,
	PLAYING
}

public class GUIController : MonoBehaviour 
{
	public static GUIController instance;
	//Public
	public GameObject HUD;
	public GameObject pauseMenu;
	public GameObject newHighscoreMenu;

	public Text scoreText;

	public Text titleText;
	public string gameNameString="Game Name";
	public string newHighscoreString="New highscore: ";
	public string highscoresString="Highscores";

	public Text lastScoreText;
	public Text yourScoreText;
	public Text[] highscoreTexts;

	public InputField newHighscoreNameField;

	void Awake ()
	{
		if (!instance) 
		{
			instance = this;
		} 
		else if (instance != this)
		{
			Destroy(gameObject);
		}
	}

	public void onPlayClick()
	{
		Time.timeScale = 1;
		GameManager.instance.startGame ();
		setGUIState (guiState.PLAYING);
	}
	
	public void onPauseClick()
	{
		setGUIState (guiState.PAUSED);
		Time.timeScale = 0;
	}

	public void onHighscoreApplyClick()
	{
		setGUIState (guiState.PAUSED);

		//Call stuff from game manager to save scores and add new score in...
		GameManager.instance.saveNewHighscore (newHighscoreNameField.text);
	}
	
	public void quitGame()
	{
		Application.Quit ();
	}

	//Menu visibilities
	public void setGUIState(guiState state)
	{
		switch (state) 
		{
		case guiState.NEW_HIGHSCORE:
			newHighscoreMenu.SetActive(true);
			HUD.SetActive(false);
			pauseMenu.SetActive(false);
			break;
		case guiState.PAUSED:
			newHighscoreMenu.SetActive(false);
			HUD.SetActive(false);
			pauseMenu.SetActive(true);
			break;
		case guiState.PLAYING:
			newHighscoreMenu.SetActive(false);
			HUD.SetActive(true);
			pauseMenu.SetActive(false);
			break;
		}
	}

	//--------------
	// 	  SCORES
	//--------------
	public void setScoreText(int score)
	{
		scoreText.text = "Score: " + score.ToString ();
	}

	public void setLastScoreText(int score)
	{
		yourScoreText.text = "Your score: " + score.ToString ();
		lastScoreText.text = "Last score: "+score.ToString (); 
	}

	public void setHighscoresText(SortedDictionary<string, int> scores)
	{
		int i = 0;
		foreach (KeyValuePair<string, int> valuePair in scores.OrderByDescending(key => key.Value)) 
		{
			if (i >= highscoreTexts.Length)
				break;

			highscoreTexts[i].text = valuePair.Key+": "+valuePair.Value;
			i++;
		}
	}

}
