﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour 
{
	private AudioSource _audioSource;

	public AudioClip buttonClick;

	void Start()
	{
		_audioSource = GetComponent<AudioSource> ();
	}

	public void playClick()
	{
		_audioSource.PlayOneShot (buttonClick);
	}

}
