﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour 
{
    public bool _draggable;
    private Renderer _renderer;
    public Rigidbody _rigidbody;
    private AudioSource _audioSource;
    private ParticleSystem _particles;

	private PlayerController _playerController;
    
	public bool isDraggable(){
		return _draggable;
	}

	public void setDraggable(bool b){
		_draggable = b;
	}

	void setRigidbodyKinematic (bool b)
	{
		_rigidbody.isKinematic = b;
	}

	void OnEnable() 
	{
        if (_renderer == null) { _renderer = gameObject.GetComponent<Renderer>(); }
        if (_rigidbody == null) { _rigidbody = gameObject.GetComponent<Rigidbody>(); }
		_draggable = true;
        _renderer.material.color = GameManager.instance.blockGradient.Evaluate (Mathf.PingPong (Time.time/25, 1));
        _renderer.material.SetColor("_EmissionColor", Color.black);

        gameObject.transform.localScale = new Vector3(Random.Range(0.5f, 2f),
                                                      Random.Range(0.5f, 2f),
                                                      Random.Range(0.5f, 2f));
		if (_rigidbody.isKinematic == true) {
			setRigidbodyKinematic(false);
		}
	}

	void OnCollisionEnter(Collision other)
	{
        if (_audioSource.enabled)
        {
            //_audioSource.pitch = Random.Range(0.75f, 1.25f);
            _audioSource.pitch = 0.25f + transform.position.y/20 + Random.Range(-0.1f, 0.1f) ;
            float velocityVolumeModif = other.relativeVelocity.magnitude / 2000;
            if (velocityVolumeModif < 0.5f)
                velocityVolumeModif = 0.5f;
            if (velocityVolumeModif > 1)
                velocityVolumeModif = 1;
            _audioSource.volume = velocityVolumeModif;

            _audioSource.PlayOneShot(_audioSource.clip);
        }
	}

    void OnCollisionStay(Collision other)
    {
        if (!_draggable || other.relativeVelocity.magnitude > 0.25f) { return; }
        if (other.gameObject.tag == "Block" || other.gameObject.tag == "Ground")
        {
            Block otherBlock = other.gameObject.GetComponent<Block>();
            if (other.gameObject.tag == "Ground" || (otherBlock && !otherBlock._draggable))
            {
                disableBlock();
            }
        }
    }

	void disableBlock()
	{
        _renderer.material.SetColor("_EmissionColor", _renderer.material.color );
        //DynamicGI.UpdateMaterials(_renderer);
        //DynamicGI.UpdateEnvironment();

        _draggable = false;
        if (transform.position.y > _playerController.lastPlacedBlock.transform.position.y)
		    _playerController.lastPlacedBlock = gameObject;

		GameManager.instance.setScore (GameManager.instance.getScore () + 1);
        _particles.Emit(Random.Range(50,100));

    }

	void Start()
	{
		_playerController = GameObject.FindObjectOfType<PlayerController> ();
		_audioSource = GetComponent<AudioSource> ();
        _renderer = GetComponent<Renderer>();
        _rigidbody = GetComponent<Rigidbody>();
        _particles = GetComponent<ParticleSystem>();
    }
}
