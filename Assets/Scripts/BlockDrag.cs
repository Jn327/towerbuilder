﻿using UnityEngine;
using System.Collections;

public class BlockDrag
{
	public Block block;
	public Rigidbody rb;
	public Touch touch;

	public BlockDrag (Block _block)
	{
		block = _block;
		rb = _block.gameObject.GetComponent<Rigidbody>();
	}

	public BlockDrag (Block _block, Touch _touch )
	{
		touch = _touch;
		block = _block;
		rb = _block.gameObject.GetComponent<Rigidbody>();
	}
}
